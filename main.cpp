#include "log.h"

#undef DEBUG_LEVEL
#define DEBUG_LEVEL 2

int main() {
    LOG(INFO,"START!\n");

    for(int i = 0; i < 10; i++) {
        LOG_I("Iterator is %d\n", i);
        if (i & 1) {
            LOG_W("Number is odd!\n");
        }
        if ((i % 4) == 0) {
            LOG_E("The answer is %d\n", 42);
        }
    }
    
    for(int i = 0; i < 10; i++) {
        LOG_IF((i % 5) == 0, "Ololo!\n");
    }

    LOG(ERROR,"END!\n");

    return 0;
}
