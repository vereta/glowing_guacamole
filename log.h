#ifndef LOG_H
#define LOG_H

#include <stdio.h>

#define RED     "\033[0;31m"
#define YELLOW  "\033[0;33m"
#define GREEN   "\033[0;32m"
#define BLUE    "\033[0;34m"
#define MAGENTA "\033[0;35m"
#define RESET   "\033[0m"

#ifndef DEBUG_LEVEl
    #define DEBUG_LEVEL 3
#endif

#define ERROR   1
#define WARNING 2
#define INFO    3

//Info
#define LOG_I(fmt, ...) do { if (DEBUG_LEVEL >= INFO) {printf(GREEN);(printf("(%7s) Line: %3d | Func: %s| Msg:" fmt, "INFO", __LINE__,__FUNCTION__, ##__VA_ARGS__));printf(RESET); }} while (0)

//Warning
#define LOG_W(fmt, ...) do { if (DEBUG_LEVEL >= WARNING) {printf(YELLOW);(printf("(%7s) Line: %3d | Func: %s| Msg:" fmt, "WARNING", __LINE__,__FUNCTION__, ##__VA_ARGS__));printf(RESET); }} while (0)

//Error
#define LOG_E(fmt, ...) do { if (DEBUG_LEVEL >= ERROR) {printf(RED);(printf("(%7s) Line: %3d | Func: %s| Msg:" fmt, "ERROR!", __LINE__,__FUNCTION__, ##__VA_ARGS__));printf(RESET); }} while (0)

//Custom severity
#define LOG(lvl, fmt, ...) do { if (DEBUG_LEVEL >= lvl) {printf(BLUE);(printf("(%7s) Line: %3d | Func: %s| Msg:" fmt, "LOG", __LINE__,__FUNCTION__, ##__VA_ARGS__));printf(RESET); }} while (0)

//Log if statement
#define LOG_IF(statement, fmt, ...) do { if (statement) {printf(MAGENTA);(printf("(%7s) Line: %3d | Func: %s| Msg:" fmt, "LOG_IF", __LINE__,__FUNCTION__, ##__VA_ARGS__));printf(RESET); }} while (0)

#endif //LOG_H
